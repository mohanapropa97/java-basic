import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        int m,n;
        System.out.print("Enter the numbers: ");
        m=in.nextInt();
        n=in.nextInt();
        System.out.print("Armstrong numbers are: ");
        for(int i=m;i<=n;i++)
        {
            int temp=i;
            double r,count=0,sum=0;;
            while(temp!=0)
            {
                temp=temp/10;
                count++;
            }
            temp=i;
            while (temp!=0)
            {
                r=temp%10;
                sum = sum + Math.pow(r,count);
                temp=temp/10;
            }
            if(sum== (double)i)
                System.out.print(i+" ");

            sum=0;
            count=0;
        }
    }
}
